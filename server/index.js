import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';

import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from '../webpack.config.js';

import api from './routes/api';
import register from './routes/register';
import auth from './routes/auth';

let app = express();

app.use(bodyParser.json());

app.use('/api/register', register);
app.use('/api/auth', auth);
app.use('/api/v1', api);

const compiler = webpack(webpackConfig);

app.use(webpackMiddleware(compiler, {
    hot: true,
    publicPath: webpackConfig.output.publicPath,
    noInfo: true
}));
app.use(webpackHotMiddleware(compiler));

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, './index.html'))
});

app.listen(3000, ()=> console.log('\x1b[33m', 'Running on localhost:3000'));